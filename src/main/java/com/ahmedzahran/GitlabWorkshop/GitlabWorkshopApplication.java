package com.ahmedzahran.GitlabWorkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabWorkshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabWorkshopApplication.class, args);
	}

}
