package com.ahmedzahran.GitlabWorkshop;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gitlab")
public class GitlabController {


    @GetMapping("/docker")
    public String printSomething(){
        return "Hello Gitlab";
    }

}